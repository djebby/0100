
from collections import deque

# GraphNode used for adjacency list
class GraphNode:
	def __init__(self, val):
		self.val = val
		self.neighbors = list()

# or use a HashMap
adjList = {'A': [], 'B': []}

# Q: given directed edges, build an adjacency list
edges = [['A', 'B'], ['B', 'C'], ['B', 'E'], ['C', 'E'], ['E', 'D']]

adjacencyList = dict()
for src, dst in edges:
	if src not in adjacencyList:
		adjacencyList[src] = list()
	if dst not in adjacencyList:
		adjacencyList[dst] = list()
	adjacencyList[src].append(dst)

print(adjacencyList)

# Count paths (backtracking)
def depthfirstsearch(node, target, adjList, visited):
	if node in visited:
		return 0
	if node == target:
		return 1
	
	visited.add(node)
	count = 0
	for neighbor in adjList[node]:
		count += depthfirstsearch(neighbor, target, adjList, visited)
	visited.remove(node)
	
	return count

print(depthfirstsearch('A', 'E', adjacencyList, set()))


def breadthfirstsearch(node, target, adjList):
	length = 0
	visited = set()
	visited.add(node)
	queue = deque()
	queue.append(node)

	while queue:
		for _ in range(len(queue)):
			curr = queue.popleft()
			if curr == target:
				return length
			for neighbor in adjList[curr]:
				if neighbor not in visited:
					visited.add(neighbor)
					queue.append(neighbor)
		length += 1
		
	return length


# Matrix DFS-----------------------------------------------------

grid = [[0, 0, 0, 0],
		[1, 1, 0, 0],
		[0, 0, 0, 1],
		[0, 1, 0, 0]]

# Q: Count the unique paths from the top left to the bottom right
#	 A single path may only move along 0's and can't visit the 
#	 same cell more than once.

# count paths (backtracking)
def dfs(grid, r, c, visited):
	ROWS, COLS = len(grid), len(grid[0])
	if(min(r, c) < 0 or r == ROWS or c == COLS 
    	or (r, c) in visited or grid[r][c] == 1):
		return 0
	if r == ROWS-1 and c == COLS-1:
		return 1
	
	visited.add((r, c))

	count = 0
	count += dfs(grid, r+1, c, visited)
	count += dfs(grid, r-1, c, visited)
	count += dfs(grid, r, c+1, visited)
	count += dfs(grid, r, c-1, visited)

	visited.remove((r, c))

	return count

print(dfs(grid, 0, 0, set()))


# Q: Find te length of the shortest path from the top left 
#	 of the grid to the bottom right.

# Shortest path from top left to bottom right
def bfs(grid):
	ROWS, COLS = len(grid), len(grid[0])
	visited = set()
	queue = deque()
	queue.append((0, 0))
	visited.add((0, 0))

	length = 0
	while queue:
		for _ in range(len(queue)):
			r, c = queue.popleft()
			if r == ROWS - 1 and c == COLS - 1:
				return length
			
			neighbors = [[0, 1], [0, -1], [1, 0], [-1, 0]]
			for dr, dc in neighbors:
				if(min(r+dr, c+dc) < 0 or r+dr == ROWS or c+dc == COLS
					or (r+dr, c+dc) in visited or grid[r+dr][c+dc] == 1):
					continue
				queue.append((r+dr, c+dc))
				visited.add((r+dr, c+dc))
		length += 1
	
print('shortest path from top left to bottom right: ', bfs(grid))