-- CREATE TABLE table_name (column name + data type + constraints if any);
CREATE TABLE person (id int, first_name VARCHAR(50), last_name VARCHAR(50), gender VARCHAR(7), date_of_birth DATE);

-- @block
DROP TABLE person;

-- @block table with constraints
CREATE TABLE person (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    gender VARCHAR(7) NOT NULL,
    date_of_birth DATE NOT NULL,
    email VARCHAR(150)
);

-- @block insertion
INSERT INTO person (first_name, last_name, gender, date_of_birth, email) 
VALUES ('Anne', 'Smith', 'FEMALE', DATE '1988-01-09', null), ('Jake', 'Jones', 'MALE', DATE '1990-01-10', 'jake@gmail.com');
